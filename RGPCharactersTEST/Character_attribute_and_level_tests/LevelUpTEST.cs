﻿using System;
using RPGCharacters;
using Xunit;

namespace RGPCharactersTEST
{
    public class LevelUpTEST
    {
        [Fact]
        public void LevelUp_LevelUpOneLevelWarrior_ReturnCorrectVitality()
        {
            //arrange
            int expected = 15;
            Warrior warrior = new Warrior("tor");
            //act
            warrior.LevelUp(1);
            int actual = warrior.primaryAttributes.VitalityPoints;
            //assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_LevelUpOneLevelWarrior_ReturnCorrectStrength()
        {
            //arrange
            int expected = 8;
            Warrior warrior = new Warrior("tor");
            //act
            warrior.LevelUp(1);
            int actual = warrior.primaryAttributes.StrenghtPoints;
            //assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_LevelUpOneLevelWarrior_ReturnCorrectDexterity()
        {
            //arrange
            int expected = 4;
            Warrior warrior = new Warrior("tor");
            //act
            warrior.LevelUp(1);
            int actual = warrior.primaryAttributes.DexterityPoints;
            //assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void LevelUp_LevelUpOneLevelWarrior_ReturnCorrectIntelligence()
        {
            //arrange
            int expected = 2;
            Warrior warrior = new Warrior("tor");
            //act
            warrior.LevelUp(1);
            int actual = warrior.primaryAttributes.IntelligencePoint;
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpOneLevel_ReturnsCorrectLevel()
        {
            //arrange 
            int expected = 2;

            //act
            Mage mage = new Mage("greg");
            mage.LevelUp(1);
            int actual = mage.Level;

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpZeroLevel_ThrowsException()
        {
            // arrange
            Mage mage = new Mage("roger");

            //act and assert
            Assert.Throws<ArgumentException>(() => mage.LevelUp(0));
        }
        [Fact]

        public void LevelUp_LevelUpOneLevelWarrior_ReturnCorrectHealth()
        {
            //arrange
            int expected = 150;
            Warrior warrior = new Warrior("tor");
            //act
            warrior.LevelUp(1);
            int actual = warrior.secondaryAttributes.Health;
            //assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void LevelUp_LevelUpOneLevelWarrior_ReturnCorrectArmorRating()
        {
            //arrange
            int expected = 12;
            Warrior warrior = new Warrior("tor");
            //act
            warrior.LevelUp(1);
            int actual = warrior.secondaryAttributes.ArmorRating;
            //assert
            Assert.Equal(expected, actual);

        }
        [Fact]
        public void LevelUp_LevelUpOneLevelWarrior_ReturnCorrectElementalResistance()
        {
            //arrange
            int expected = 2;
            Warrior warrior = new Warrior("tor");
            //act
            warrior.LevelUp(1);
            int actual = warrior.secondaryAttributes.ElementalResistance;
            //assert
            Assert.Equal(expected, actual);

        }
    }
}
