using System;
using Xunit;
using RPGCharacters;

namespace RGPCharactersTEST
{
    public class RPGcharachterCreationTEST
    {
       

        [Fact]
        public void MageCreation_ShouldHave_Level1Streangth()
        {
            // arrange 
            Mage mage = new Mage("fil");
            int expected = 1;
            //act
            int actual = mage.primaryAttributes.StrenghtPoints;
            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerCreation_ShouldHave_Level1Streangth()
        {
            // arrange 
            Ranger ranger = new Ranger("fil");
            int expected = 1;
            //act
            int actual = ranger.primaryAttributes.StrenghtPoints;
            // assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void RougeCreation_ShouldHave_Level1Intellegence()
        {
            // arrange 
            Rouge rouge = new Rouge("fil");
            int expected = 1;
            //act
            int actual = rouge.primaryAttributes.IntelligencePoint;
            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorCreation_ShouldHave_Level1Intellegence()
        {
            // arrange 
            Warrior warrior = new Warrior("fil");
            int expected = 1;
            //act
            int actual = warrior.primaryAttributes.IntelligencePoint;
            // assert
            Assert.Equal(expected, actual);
        }


    }
}
