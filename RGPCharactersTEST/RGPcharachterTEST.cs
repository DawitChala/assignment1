﻿using System;
using RPGCharacters;
using Xunit;

namespace RGPCharactersTEST
{
    public class EquippingArmourTEST
    {

        [Fact]
        public void GetDPS_WithoutAnyItems_returnsNumber()
        {
            //arrange
            Warrior warrior = new("tor");
            double expected = 1.00 * (1.00 + (5.00 / 100.00));
            //act
            double actual = warrior.getDPS();
            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDPS_WithWeapon_returnsNumber()
        {
            //arrange
            Warrior warrior = new("tor");
            double expected = (7.00 * 1.10) * (1.00 + (5.00 / 100.00));

            RPGCharacters.items.Weapons testAxe = new RPGCharacters.items.Weapons
            {
                Type = RPGCharacters.items.Weapons.WeaponType.WEAPON_AXE,
                ItemName = "hei",
                ItemLevel = 1,
                ItemSlot = RPGCharacters.items.Items.Slot.Slot_Weapon,
                WeaponAttributes = new RPGCharacters.items.WeaponAttributes { Damage = 7, AttackSpeed = 1.1 }
            };
            //act
            warrior.EquipWeapon(testAxe);
            double actual = warrior.getDPS();
            //assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void GetDPS_WithWeaponAndArmor_returnsNumber()
        {
            //arrange
            Warrior warrior = new("tor");
            double expected = (7.00 * 1.10) * (1.00 + ((5.00 + 1.00) / 100.00));

            RPGCharacters.items.Weapons testAxe = new RPGCharacters.items.Weapons
            {
                Type = RPGCharacters.items.Weapons.WeaponType.WEAPON_AXE,
                ItemName = "hei",
                ItemLevel = 1,
                ItemSlot = RPGCharacters.items.Items.Slot.Slot_Weapon,
                WeaponAttributes = new RPGCharacters.items.WeaponAttributes { Damage = 7, AttackSpeed = 1.1 }
            };


            RPGCharacters.items.Armor testPlate = new RPGCharacters.items.Armor
            {
                ArmourType = RPGCharacters.items.Armor.Armour.ARMOR_PLATE,
                ItemName = "hei",
                ItemLevel = 1,
                ItemSlot = RPGCharacters.items.Items.Slot.Slot_Body,
                Attributes = new PrimaryAttributes { VitalityPoints = 2, StrenghtPoints=1 }

            };

            //act
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlate);
            double actual = warrior.getDPS();
            //assert
            Assert.Equal(expected, actual);
        }



        [Fact]
        public void WieldWeapon_UseToHighLevelWeapon_ShouldThrowException()
        {
            //arrange
            RPGCharacters.items.Weapons testAxe = new RPGCharacters.items.Weapons
            {
                Type = RPGCharacters.items.Weapons.WeaponType.WEAPON_AXE,
                ItemName = "hei",
                ItemLevel = 2,
                ItemSlot = RPGCharacters.items.Items.Slot.Slot_Weapon,
                WeaponAttributes = new RPGCharacters.items.WeaponAttributes{ Damage= 2, AttackSpeed = 1.2}
            };

            Warrior warrior = new("tor");

            //act & assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }
        [Fact]
        public void WieldWeapon_UseToHighLevelArmor_ShouldThrowException()
        {
            //arrange
            RPGCharacters.items.Armor testPlate = new RPGCharacters.items.Armor
            {
                ArmourType = RPGCharacters.items.Armor.Armour.ARMOR_PLATE,
                ItemName = "hei",
                ItemLevel = 2,
                ItemSlot = RPGCharacters.items.Items.Slot.Slot_Body,
                Attributes = new PrimaryAttributes { VitalityPoints= 5}

            };

            Warrior warrior = new("tor");

            //act & assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlate));
        }
        [Fact]
        public void WieldWeapon_UseWrongWeapon_ShouldThrowException()
        {
            //arrange
            RPGCharacters.items.Weapons testBow = new()
            {
                Type = RPGCharacters.items.Weapons.WeaponType.WEAPON_BOW,
                ItemName = "hei",
                ItemLevel = 1,
                ItemSlot = RPGCharacters.items.Items.Slot.Slot_Weapon,
                WeaponAttributes = new RPGCharacters.items.WeaponAttributes { Damage = 2, AttackSpeed = 1.2 }
            };

            Warrior warrior = new("tor");

            //act & assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }
        [Fact]
        public void WieldWeapon_UseCorrectWeapon_ShouldReturnString()
        {
            //arrange
            RPGCharacters.items.Weapons testAxe = new RPGCharacters.items.Weapons
            {
                Type = RPGCharacters.items.Weapons.WeaponType.WEAPON_AXE,
                ItemName = "hei",
                ItemLevel = 1,
                ItemSlot = RPGCharacters.items.Items.Slot.Slot_Weapon,
                WeaponAttributes = new RPGCharacters.items.WeaponAttributes { Damage = 2, AttackSpeed = 1.2 }
            };
            string expected = "New weapon equipped!";
            Warrior warrior = new("tor");
            //act 
            string actual = warrior.EquipWeapon(testAxe);
            //assert
            Assert.Equal(expected, actual); 
        }
    }
}
