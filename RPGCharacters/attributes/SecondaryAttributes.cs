﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class SecondaryAttributes
    {
        PrimaryAttributes primaryAttributes;
        public int Health { get => primaryAttributes.VitalityPoints*10; }
        public int ArmorRating { get => primaryAttributes.StrenghtPoints+primaryAttributes.DexterityPoints; }
        public int ElementalResistance { get => primaryAttributes.IntelligencePoint; }

        public SecondaryAttributes(PrimaryAttributes primaryAttributes)
        {
            this.primaryAttributes = primaryAttributes;

        }
    }
}
