﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class PrimaryAttributes
    {
        public int StrenghtPoints { get; set; }
        public int DexterityPoints { get; set; }
        public int IntelligencePoint { get; set; }
        public int VitalityPoints { get; set; }
        

        /// <summary>
        /// Ovveride the plus operator, so that you can add two attributes togheter.
        /// </summary>
        /// <param name="primaryAttributes"></param>
        /// <param name="upgrade"></param>
        /// <returns>new primaryattributes</returns>
        public static PrimaryAttributes operator +(PrimaryAttributes primaryAttributes, PrimaryAttributes upgrade)
        {
            return new PrimaryAttributes
            {
                StrenghtPoints = primaryAttributes.StrenghtPoints + upgrade.StrenghtPoints,
                DexterityPoints = primaryAttributes.DexterityPoints + upgrade.DexterityPoints,
                IntelligencePoint = primaryAttributes.IntelligencePoint + upgrade.IntelligencePoint,
                VitalityPoints = primaryAttributes.VitalityPoints + upgrade.VitalityPoints
            };

        }

    }
}
