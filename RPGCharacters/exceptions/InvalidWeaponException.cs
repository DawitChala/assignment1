﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(): base("Not possible to wield this weapon")
        { }
    }
}
