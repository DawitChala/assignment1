﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class InvalidArmorException: Exception
    {
        public InvalidArmorException() : base("Not possible to wield this armour")
        { }
    }
}
