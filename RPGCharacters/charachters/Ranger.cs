﻿
using RPGCharacters.items;

namespace RPGCharacters
{
    public class Ranger : Charachter 
    {
        
        public Ranger(string name):base(name)
        {
            upgradeValues = new PrimaryAttributes {StrenghtPoints = 1,  VitalityPoints = 2,
                DexterityPoints = 5, IntelligencePoint = 1
            };

            primaryAttributes = new PrimaryAttributes {StrenghtPoints = 1, VitalityPoints = 8,
                DexterityPoints = 7, IntelligencePoint = 1
            };
            AttackSpeed = 1;
            Damage = 1;
        }
        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmourType != Armor.Armour.ARMOR_LEATHER && armor.ArmourType != Armor.Armour.ARMOR_MAIL)
                throw new InvalidArmorException();
            if(armor.ItemLevel > Level)
                throw new InvalidArmorException();
            EquipedItems[armor.ItemSlot] = armor;
            return "New armor equipped!";
        }

        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.Type != Weapons.WeaponType.WEAPON_BOW || weapon.ItemLevel > Level)
                throw new InvalidWeaponException();

            AttackSpeed = weapon.WeaponAttributes.AttackSpeed;
            Damage = weapon.WeaponAttributes.Damage;

            EquipedItems[weapon.ItemSlot] = weapon;
            return "New weapon equipped!";
        }
        public override double getDPS()
        {
            CalculateArmorBoost();
            return (AttackSpeed * Damage) * (1.00 + (WithArmorUpgrades.VitalityPoints / 100.00));
        }

    }
}
