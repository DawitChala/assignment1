﻿using RPGCharacters.items;
using System.Collections.Generic;


namespace RPGCharacters
{
    public class Mage : Charachter
    {
        
        public Mage(string name) : base (name)
        {
            upgradeValues = new PrimaryAttributes { StrenghtPoints = 1, VitalityPoints = 3, DexterityPoints = 1, IntelligencePoint = 5};
            primaryAttributes = new PrimaryAttributes { StrenghtPoints = 1, VitalityPoints = 5, DexterityPoints = 1, IntelligencePoint = 8 };
            AttackSpeed = 1;
            Damage = 1;
        }
        
        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmourType != Armor.Armour.ARMOR_CLOTH || armor.ItemLevel > Level)
                throw new InvalidArmorException();
            EquipedItems[armor.ItemSlot] = armor;
            return "New armor equipped!";
        }

        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.Type != Weapons.WeaponType.WEAPON_STAFF && weapon.Type != Weapons.WeaponType.WEAPON_WAND)
                throw new InvalidWeaponException();
            if (weapon.ItemLevel > Level)
                throw new InvalidWeaponException();

            EquipedItems[weapon.ItemSlot] = weapon;
            AttackSpeed = weapon.WeaponAttributes.AttackSpeed;
            Damage = weapon.WeaponAttributes.Damage;
            return "New weapon equipped!";
        }
        public override double getDPS()
        {
            CalculateArmorBoost();
            return (AttackSpeed * Damage) * (1.00 + (WithArmorUpgrades.IntelligencePoint / 100.00));
        }


    }
}
