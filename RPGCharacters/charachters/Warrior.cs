﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacters.items;

namespace RPGCharacters
{
    public class Warrior : Charachter
    {

        public Warrior(string name) :  base (name)
        {
            upgradeValues = new PrimaryAttributes{ StrenghtPoints = 3, VitalityPoints = 5,
                DexterityPoints = 2, IntelligencePoint = 1};

            primaryAttributes = new PrimaryAttributes{ StrenghtPoints = 5, VitalityPoints = 10,
                DexterityPoints = 2,IntelligencePoint = 1};

            AttackSpeed = 1;
            Damage = 1;

        }

        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmourType != Armor.Armour.ARMOR_PLATE && armor.ArmourType != Armor.Armour.ARMOR_MAIL)
                throw new InvalidArmorException();

            if (armor.ItemLevel > Level)
                throw new InvalidArmorException();

            EquipedItems[armor.ItemSlot] = armor;
            return "New armor equipped!";
        }

        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.Type != Weapons.WeaponType.WEAPON_AXE && weapon.Type != Weapons.WeaponType.WEAPON_SWORD
                && weapon.Type != Weapons.WeaponType.WEAPON_HAMMER)

                throw new InvalidWeaponException();

            if (weapon.ItemLevel > Level)
                throw new InvalidWeaponException();

            AttackSpeed = weapon.WeaponAttributes.AttackSpeed;
            Damage = weapon.WeaponAttributes.Damage;
            EquipedItems[weapon.ItemSlot] = weapon;
            return "New weapon equipped!";

        }
        public override double getDPS()
        {
            CalculateArmorBoost();
            return (AttackSpeed * Damage) * (1.00 + (WithArmorUpgrades.StrenghtPoints / 100.00));
        }
    }
}
