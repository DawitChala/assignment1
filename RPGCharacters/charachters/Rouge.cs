﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGCharacters.items;

namespace RPGCharacters
{
    public class Rouge : Charachter
    {
        public Rouge(string name) : base(name)
        {
            upgradeValues = new PrimaryAttributes { StrenghtPoints = 1, VitalityPoints = 3,
                DexterityPoints = 4, IntelligencePoint = 1
            };

            primaryAttributes = new PrimaryAttributes  { StrenghtPoints = 2, VitalityPoints = 8,
                DexterityPoints = 6, IntelligencePoint = 1
            };
            AttackSpeed = 1;
            Damage = 1;

        }
        public override string EquipArmor(Armor armor)
        {
            if (armor.ArmourType != Armor.Armour.ARMOR_CLOTH && armor.ArmourType != Armor.Armour.ARMOR_MAIL)
                throw new InvalidArmorException();
            if (armor.ItemLevel > Level)
                throw new InvalidArmorException();
            EquipedItems[armor.ItemSlot] = armor;
            return "New armor equipped!";
        }

        public override string EquipWeapon(Weapons weapon)
        {
            if (weapon.Type != Weapons.WeaponType.WEAPON_DAGGER && weapon.Type != Weapons.WeaponType.WEAPON_SWORD   )
                throw new InvalidWeaponException();
            if (weapon.ItemLevel > Level)
                throw new InvalidWeaponException();
            AttackSpeed = weapon.WeaponAttributes.AttackSpeed;
            Damage = weapon.WeaponAttributes.Damage;
            EquipedItems[weapon.ItemSlot] = weapon;
            return "New weapon equipped!";
        }
        public override double getDPS()
        {
            CalculateArmorBoost();
            return (AttackSpeed * Damage) * (1.00 + (WithArmorUpgrades.VitalityPoints / 100.00));
        }

    }
}
