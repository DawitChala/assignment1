﻿using System;
using System.Collections.Generic;
using RPGCharacters.items;


namespace RPGCharacters
{

    public abstract class Charachter
    {
        public PrimaryAttributes primaryAttributes { get; set; }
        public PrimaryAttributes upgradeValues;
        public PrimaryAttributes WithArmorUpgrades { get; set; }
        public SecondaryAttributes secondaryAttributes { get; set; }
        public string Name { get; }
        public int Level { get; set; }
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
        public Dictionary<Items.Slot, Items> EquipedItems = new Dictionary<Items.Slot, Items>();
        public Charachter(string name)
        {
            this.Name = name;
            Level = 1;
            secondaryAttributes = new SecondaryAttributes(primaryAttributes);
            WithArmorUpgrades = new PrimaryAttributes();

        }
        /// <summary>
        /// Method to level up hero
        /// </summary>
        /// <param name="levels"></param>
        public void LevelUp(int levels)
        {
            if (levels < 1)
            {
                throw new ArgumentException();
            }
            for (int i = 0 ; i < levels; i++)
            {
                primaryAttributes = primaryAttributes + upgradeValues;
                Level = Level + 1;
                secondaryAttributes = new SecondaryAttributes(primaryAttributes);
            }
        }

        /// <summary>
        /// calculates the bonus the armor gives the hero
        /// </summary>
        public void CalculateArmorBoost()
        {
            WithArmorUpgrades.DexterityPoints = primaryAttributes.DexterityPoints;
            WithArmorUpgrades.StrenghtPoints = primaryAttributes.StrenghtPoints;
            WithArmorUpgrades.IntelligencePoint = primaryAttributes.IntelligencePoint;
            WithArmorUpgrades.VitalityPoints = primaryAttributes.VitalityPoints;

            foreach (Items item in EquipedItems.Values)
            {
                if (item.ItemSlot != Items.Slot.Slot_Weapon)
                {
                    WithArmorUpgrades = item.Attributes + WithArmorUpgrades;

                }
            }

        }

        public string PresentCharachter()
        {
            CalculateArmorBoost();
            return $"Charachter name: {Name} \n Charachter level: {Level}" +
                $" \n Strength: {WithArmorUpgrades.StrenghtPoints} \n" +
                $" Dexterity: {WithArmorUpgrades.DexterityPoints} \n " +
                $"Intelligence: {WithArmorUpgrades.DexterityPoints} \n" +
                $"Health: {WithArmorUpgrades} \n" +
                $"Armor Rating: {secondaryAttributes.ArmorRating}\n" +
                $"DPS: {getDPS()}";
        }
        /// <summary>
        /// Equip weapon if the weapon is valid
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>sucsess or throws exception</returns>
        public abstract string EquipWeapon(Weapons weapon);

        /// <summary>
        /// Equip weapon if the weapon is valid
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>sucsess or throws exception</returns>
        public abstract string EquipArmor(Armor armor);

        /// <summary>
        /// Calculates the DPS
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>double DPS</returns>
        public abstract double getDPS();

    }
}
