﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.items
{
    

    public class Weapons : Items
    {
        public enum WeaponType { WEAPON_BOW, WEAPON_STAFF, WEAPON_SWORD,
            WEAPON_WAND, WEAPON_DAGGER,WEAPON_HAMMER, WEAPON_AXE };

        public WeaponType Type { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }


    }
}
