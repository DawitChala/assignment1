﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.items
{
    
    public class Armor: Items
    {
        public enum Armour { ARMOR_CLOTH, ARMOR_LEATHER, ARMOR_MAIL, ARMOR_PLATE }
        public Armour ArmourType { get; set; }
        
    }
}
