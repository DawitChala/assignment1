﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.items
{
    public abstract class Items
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public enum Slot { Slot_Weapon, Slot_Body, Slot_Head, Slot_Legs }
        public Slot ItemSlot { get; set; }
        public PrimaryAttributes Attributes { get; set; }

        public Items()
        {
        }
    }
}
